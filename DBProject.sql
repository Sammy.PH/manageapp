CREATE TABLE institution(
institution_id int NOT NULL AUTO_INCREMENT,
name varchar(7),
phone_number varchar(100),
interest_rate varchar(7),
primary key (institution_id));

CREATE TABLE mortgage(
mortgage_id int NOT NULL AUTO_INCREMENT,
institution_id int Null,
start_date date,
end_date date,
property_id int,
primary key (mortgage_id),
foreign key (institution_id) references institution(institution_id));

CREATE TABLE properties(
property_id int NOT NULL AUTO_INCREMENT,
address varchar(100),
postal_code varchar(100),
mortgage_id int Null,
number_of_unit int,
property_value int,
primary key (property_id),
foreign key (mortgage_id) references mortgage(mortgage_id) ON DELETE CASCADE) ;

CREATE TABLE lease(
lease_id int NOT NULL AUTO_INCREMENT,
tenant_id int,
start_date date,
end_date date,
renew bool,
contract blob,
primary key (lease_id));

CREATE TABLE user(
user_id int NOT NULL AUTO_INCREMENT,
username varchar(100) UNIQUE,
password varchar(100),
email varchar(100),
role varchar(100),
primary key(user_id));

CREATE TABLE landlord(
landlord_id  int NOT NULL AUTO_INCREMENT,
first_name VARCHAR(100),
last_name VARCHAR(100),
email varchar(100),
phone_number varchar(10),
user_id int,
primary key (landlord_id),
foreign key (user_id) references user(user_id) ON DELETE CASCADE);

CREATE TABLE tenants(
tenant_id int NOT NULL AUTO_INCREMENT,
first_name varchar(100),
last_name VARCHAR(100),
email varchar(100),
phone_number varchar(10),
unit_id int,
lease_id int,
user_id int,
overdue_days char(10),
primary key (tenant_id),
foreign key (lease_id) references lease(lease_id) ON DELETE CASCADE,
foreign key (user_id) references user(user_id) ON DELETE CASCADE);

CREATE TABLE payments(
payment_id INT NOT NULL AUTO_INCREMENT,
rent VARCHAR(7),
due_date date,
tenant_id varchar(7),
paid bool,
primary key (payment_id));

CREATE TABLE units(
unit_id int NOT NULL AUTO_INCREMENT,
unit_number int,
property_id int Null,
tenant_id int Null,
maintenance_unit_id int,
rent varchar(7),
primary key (unit_id),
foreign key (property_id) references properties(property_id) ON DELETE CASCADE,
foreign key (tenant_id) references tenants(tenant_id) ON DELETE CASCADE);

CREATE TABLE propertiesunits(
property_id int,
unit_id int,
foreign key (property_id) references properties(property_id) ON DELETE CASCADE,
foreign key (unit_id) references units(unit_id) ON DELETE CASCADE);

CREATE TABLE contractors(
contractor_id int NOT NULL AUTO_INCREMENT,
name varchar(100),
phone_number varchar(100),
maintenance_id int,
primary key (contractor_id));

CREATE TABLE maintenance(
maintenance_id int NOT NULL AUTO_INCREMENT,
issue_type varchar(100),
contractor_id int,
date_issue date,
date_fixed date,
primary key (maintenance_id),
foreign key (contractor_id) references contractors(contractor_id) ON DELETE CASCADE);

CREATE TABLE maintenanceunits(
maintenance_unit_id int NOT NULL AUTO_INCREMENT,
unit_id int,
maintenance_id int,
primary key (maintenance_unit_id),
foreign key (unit_id) references units(unit_id) ON DELETE CASCADE,
foreign key (maintenance_id) references maintenance(maintenance_id) ON DELETE CASCADE
);

/*This event will run resetPaid procedure. It will reset every tenant paid to false for the next payment*/
DELIMITER *

CREATE EVENT newRent
ON SCHEDULE EVERY 3 WEEK
DO
	call monthlyRent;
END *