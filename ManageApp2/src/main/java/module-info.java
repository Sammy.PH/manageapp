module application {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires java.base;

    opens application to javafx.fxml;
    opens tools to java.base;
 
    exports application;
    exports tools;
}
