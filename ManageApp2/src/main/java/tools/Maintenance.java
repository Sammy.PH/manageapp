package tools;

public class Maintenance {
    private String unitId;
    private String maintenanceId;
    private String issue;
    private String dateStar;
    private String dateResolve;
    private String name;
    private String phone;

    public Maintenance(String unitId, String maintenanceId, String issue, String dateStar, String dateResolve, String name, String phone) {
        this.unitId = unitId;
        this.maintenanceId = maintenanceId;
        this.issue = issue;
        this.dateStar = dateStar;
        this.dateResolve = dateResolve;
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getMaintenanceId() {
        return maintenanceId;
    }

    public void setMaintenanceId(String maintenanceId) {
        this.maintenanceId = maintenanceId;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public String getDateStar() {
        return dateStar;
    }

    public void setDateStar(String dateStar) {
        this.dateStar = dateStar;
    }

    public String getDateResolve() {
        return dateResolve;
    }

    public void setDateResolve(String dateResolve) {
        this.dateResolve = dateResolve;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    
}
