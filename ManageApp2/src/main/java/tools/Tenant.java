package tools;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Tenant extends User {
    private String address;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String unitID;
    private String leaseID;
    
    //The constructor will fetch all tenant's information and populate its private fields
    public Tenant(java.sql.Connection connection, String username){
        super(connection, username);
        try {
            //Fetching tenant information
            String query = "SELECT first_name, last_name, phone_number, unit_id, lease_id  FROM tenants " + 
                            "JOIN user ON tenants.user_id = user.user_id " +
                            "WHERE username = ?";
            PreparedStatement fetchTenant = connection.prepareStatement(query);
            fetchTenant.setString(1, username);
            ResultSet tenantResult = fetchTenant.executeQuery();
            tenantResult.next();
            this.firstName = tenantResult.getString("first_name");
            this.lastName = tenantResult.getString("last_name");
            this.phoneNumber = tenantResult.getString("phone_number");
            this.unitID = tenantResult.getString("unit_id");
            this.leaseID = tenantResult.getString("lease_id");
            fetchTenant.close();
            tenantResult.close();
            
            //fetching address
            String addressQuery = "SELECT address FROM properties " +
                                    "JOIN  propertiesunits ON properties.property_id = propertiesunits.property_id " +
                                    "JOIN units ON propertiesunits.unit_id = units.unit_id " +
                                    "JOIN tenants ON units.unit_id = tenants.unit_id " +
                                    "JOIN user ON tenants.user_id = user.user_id " +
                                    "WHERE user.username = ?";
            PreparedStatement fetchAddress = connection.prepareStatement(addressQuery);
            fetchAddress.setString(1, username);
            ResultSet addressResult = fetchAddress.executeQuery();
            addressResult.next();
            this.address = addressResult.getString("address");
        } catch (SQLException ex) {
            Logger.getLogger(Tenant.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getAddress(){
        return this.address;
    }
    
    public String getName(){
        return this.firstName + " " + this.lastName;
    }
}
