package tools;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Switch {
    
	public void switchScene(ActionEvent event, String fxml) {
            String fxmlPath = "/application/" + fxml;
                try {
                    Parent root = FXMLLoader.load(getClass().getResource(fxmlPath));
                    Scene newScene = new Scene(root);
                    Stage Window = (Stage) ((Node)event.getSource()).getScene().getWindow();
			
                    Window.setScene(newScene);
		}
		catch(Exception e) {
                    e.printStackTrace();
		}
		
	}
        
        //Switching scene with data
        public void switchScene(ActionEvent event, String fxml, Parent root) {
            String fxmlPath = "/application/" + fxml;
                try {
                    Scene newScene = new Scene(root);
                    Stage Window = (Stage) ((Node)event.getSource()).getScene().getWindow();
			
                    Window.setScene(newScene);
		}
		catch(Exception e) {
                    e.printStackTrace();
		}
		
	}
}
