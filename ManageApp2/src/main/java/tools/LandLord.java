package tools;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LandLord extends User{
    private String firstName;
    private String lastName;
    
    public LandLord(java.sql.Connection connection, String username){
        super(connection, username);
        String query = "SELECT first_name, last_name  FROM landlord " + 
                            "JOIN user ON landlord.user_id = user.user_id " +
                            "WHERE username = ?";
        try{
            PreparedStatement fetchLandlord = connection.prepareStatement(query);
            fetchLandlord.setString(1, username);
            ResultSet LandlordResult = fetchLandlord.executeQuery();
            LandlordResult.next();
            this.firstName = LandlordResult.getString("first_name");
            this.lastName = LandlordResult.getString("last_name");
            fetchLandlord.close();
            LandlordResult.close();
        }catch (SQLException ex){
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getName(){
        return this.firstName + " " + this.lastName;
    }
}
