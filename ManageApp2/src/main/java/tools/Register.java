package tools;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.text.Text;

public class Register extends saltHash{
    private java.sql.Connection con;
	
    public Register(java.sql.Connection con) {
        this.con = con;
    }
    
    public void registerUser(String password, String user, String lastname, String firstname, String email, Text promptMessage){
        String salt = getSalt();
        byte[] hashed = hash(password, salt);
        try {
            String query = "INSERT INTO users VALUES(?,?,?,?,?, ?, 1)";
            PreparedStatement createUser = this.con.prepareStatement(query);
            createUser.setString(1, user);
            createUser.setString(2, salt);
            createUser.setBytes(3, hashed);
            createUser.setString(4, lastname);
            createUser.setString(5, firstname);
            createUser.setString(6, email);
            createUser.executeUpdate();
            promptMessage.setText("You have successfully registered. You may go back to the login");
        } catch (SQLException ex) {
            Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
            promptMessage.setText("Please fill all asked information and try again");
        }
    }
}
