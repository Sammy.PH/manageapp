package tools;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import tools.Institution;

public class Mortgage{
    private int mortgageId;
    private int instId;
    private String startDate;
    private String endDate;
    private int propId;

    public Mortgage(int mortgageId, int instId, String startDate, String endDate, int propId) {
        this.mortgageId = mortgageId;
        this.instId = instId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.propId = propId;
    }
        
    public Mortgage(int instId) {
        this.instId = instId;
    }

    public int getMortgageId() {
        return mortgageId;
    }

    public void setMortgageId(int mortgageId) {
        this.mortgageId = mortgageId;
    }

    public int getInstId() {
        return instId;
    }

    public void setInstId(int instId) {
        this.instId = instId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getPropId() {
        return propId;
    }

    public void setPropId(int propId) {
        this.propId = propId;
    }
    
    public int addMortgage() throws SQLException{
        Connection connection = new Connection();
        String query = "INSERT INTO mortgage values(null, ?,CURRENT_DATE(), DATE_ADD(CURRENT_DATE(), INTERVAL 5 YEAR), null)";
        String[] column = new String[]{"mortgage_id"};
        PreparedStatement statement = connection.getConnection().prepareStatement(query, column);
        statement.setInt(1, this.instId);
        int row = statement.executeUpdate();
        if(row == 0){
            throw new SQLException("Mortgage not added");
        }
        ResultSet rs = statement.getGeneratedKeys();//Getting the id generated for the mortgage
        rs.next();
        int mortgageId = rs.getInt(1);
        return mortgageId;
    }
}
