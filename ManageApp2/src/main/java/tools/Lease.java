/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.sql.Blob;

/**
 *
 * @author tcvsh
 */
public class Lease {
    private int leaseId;
    private int tenant_id;
    private String start_date;
    private String end_date;
    private boolean renew;
    private Blob contract;

    public Lease(int leaseId, int tenant_id, String start_date, String end_date, boolean renew, Blob contract) {
        this.leaseId = leaseId;
        this.tenant_id = tenant_id;
        this.start_date = start_date;
        this.end_date = end_date;
        this.renew = renew;
        this.contract = contract;
    }

    public int getLeaseId() {
        return leaseId;
    }

    public void setLeaseId(int leaseId) {
        this.leaseId = leaseId;
    }

    public int getTenant_id() {
        return tenant_id;
    }

    public void setTenant_id(int tenant_id) {
        this.tenant_id = tenant_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public boolean isRenew() {
        return renew;
    }

    public void setRenew(boolean renew) {
        this.renew = renew;
    }

    public Blob getContract() {
        return contract;
    }

    public void setContract(Blob contract) {
        this.contract = contract;
    }
    
    
}
