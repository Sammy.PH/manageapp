package tools;

public class Institution {
    private String property;
    private int institutionId;
    private String name;
    private String address;
    private String phone;
    private String rate;

    public Institution(String property, String name, String address, String phone, String rate) {
        this.property = property;
        this.name = name;
        this.phone = phone;
        this.rate = rate;
    }

    public Institution(int institutionId, String name, String phone, String rate) {
        this.institutionId = institutionId;
        this.name = name;
        this.phone = phone;
        this.rate = rate;
    }
    

    public int getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(int institutionId) {
        this.institutionId = institutionId;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }
    
    
}
