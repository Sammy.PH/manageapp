package tools;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class User {
   private String username;
   private String password;
   private String email;
   private String userID;
   
   public User(java.sql.Connection connection, String username ){
       String query = "SELECT user_id, password, email FROM user WHERE username = ?";
       try {
           PreparedStatement user = connection.prepareStatement(query);
           user.setString(1, username);
           ResultSet rs = user.executeQuery();
           rs.next();
           this.userID = rs.getString("user_id");
           this.password = rs.getString("password");
           this.email = rs.getString("email");
           this.username = username;
           rs.close();
           user.close();
       } catch (SQLException ex) {
           Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
       }
       
   }
   
   public String getUsername(){
        return this.username;
   }
   
   public String getPassword(){
        return this.password;
   }
   
   public String getEmail(){
        return this.email;
    }
   
   public String getUserID(){
       return this.userID;
   }
}
