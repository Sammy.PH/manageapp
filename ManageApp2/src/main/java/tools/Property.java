/*
 * object that illustrate all properties
 */
package tools;

import javafx.beans.property.IntegerProperty;
import javafx.scene.control.Button;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Property {
    String propertyID;
    String address;
    String postalCode;
    String mortgageId;
    String noOfUnit;
    String propertyValue;

    public Property(String propertyID, String address, String postalCode, String mortgageId, String noOfUnit, String propertyValue) {
        this.propertyID = propertyID;
        this.address = address;
        this.postalCode = postalCode;
        this.mortgageId = mortgageId;
        this.noOfUnit = noOfUnit;
        this.propertyValue = propertyValue;
    }
    
    
    //Return methods for propertyid
    
    public void setPropId(String propId){
        this.propertyID = propId;
    }
    
    //return methods for address
    
    public void setAddress(String address){
        this.address = address;
    }
    
    //return methods for posta
    
    public void setPostal(String postal){
        this.postalCode = postal;
    }

    public String getPropertyID() {
        return propertyID;
    }

    public String getAddress() {
        return address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getMortgageId() {
        return this.mortgageId;
    }

    public String getNoOfUnit() {
        return noOfUnit;
    }

    public String getPropertyValue() {
        return propertyValue;
    }
    
    //return methods for mortgageid
    
    public void setMortgageId(String mortgageId){
        this.mortgageId = mortgageId;
    }
    //return methods for no of units
    
    public void setnoUnit(String noUnits){
        this.noOfUnit = noUnits;
    }
    
    //return methods for prop values
    
    public void setPropValue(String value){
        this.propertyValue = value;
    }
    
   //add Property
    public void addProperty(Institution inst) throws SQLException{
        Connection connection = new Connection();
        int instId = inst.getInstitutionId();
        String query = "INSERT INTO mortgage values(null, ?,CURRENT_DATE(), DATE_ADD(CURRENT_DATE(), INTERVAL 5 YEAR), null)";
        String[] column = new String[]{"mortgage_id"};
        PreparedStatement statement = connection.getConnection().prepareStatement(query, column);
        statement.setInt(1, instId);
        int row = statement.executeUpdate();
        
    }
    
    public void deleteProperty() throws SQLException{
        Connection connection = new Connection();
        String query = "DELETE FROM properties WHERE property_id = ?";
        PreparedStatement statement = connection.getConnection().prepareStatement(query);
        statement.setInt(1, Integer.valueOf(this.propertyID));
        statement.executeUpdate();
    }

}
