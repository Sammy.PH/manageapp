package tools;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class Login extends saltHash{
    private java.sql.Connection con;
	
    public Login(java.sql.Connection con) {
		this.con = con;
	}
	
	public int validate(String user, String password) {
		String query = "SELECT password FROM user WHERE username = ?";
		try {
			PreparedStatement checkUser = this.con.prepareStatement(query);
			checkUser.setString(1, user);
			ResultSet rs = checkUser.executeQuery();
			rs.next();
			String fetchPassword = rs.getString("password");
			rs.close();
			//Calling a procedure to verify if the username and password match
                        String statement = "{call verifyLogin(?,?,?)}";
			CallableStatement compare = this.con.prepareCall(statement);
			compare.setString(1, fetchPassword);
			compare.setString(2, user);
                        compare.registerOutParameter(3, Types.INTEGER);
			compare.execute();
			int verified = compare.getInt(3);
                        System.out.println(verified);
			compare.close();
                        return verified;//if verified = 1, it means its a normal user, 5 for an admin and 0 for no match
		}
		catch(SQLException e) {
			System.out.println("Missing inputs");
			return 0;
		}
	}
}
