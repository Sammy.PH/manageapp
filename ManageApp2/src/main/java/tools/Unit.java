package tools;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import tools.Connection;
public class Unit {
    private int unitId;
    private String tenantName;
    private String email;
    private String leaseDate;
    private Boolean paid;
    private String phone;
    private String overdue;

    public Unit(int unitId, String tenantName, String email, String leaseDate, Boolean paid, String phone, String overdue) {
        this.unitId = unitId;
        this.tenantName = tenantName;
        this.email = email;
        this.leaseDate = leaseDate;
        this.paid = paid;
        this.phone = phone;
        this.overdue = overdue;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLeaseDate() {
        return leaseDate;
    }

    public void setLeaseDate(String leaseDate) {
        this.leaseDate = leaseDate;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOverdue() {
        return overdue;
    }

    public void setOverdue(String overdue) {
        this.overdue = overdue;
    }


    public void deleteUnit() throws SQLException{
        String query = "DELETE FROM units WHERE unit_id = ?";
        Connection connection = new Connection();
        PreparedStatement deleteUnit = connection.getConnection().prepareStatement(query);
        deleteUnit.setInt(1, this.unitId);
        deleteUnit.executeUpdate();
    }
    
}