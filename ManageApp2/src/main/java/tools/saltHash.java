package tools;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class saltHash {
    private static SecureRandom random = new SecureRandom();
	
    //Since those two methods are going to be used by two other classes, I decided to make it a "parent" class for the other two to inherit it
    public byte[] hash(String password, String salt) {
    try{
        PBEKeySpec spec = new PBEKeySpec((password+salt).toCharArray());
	SecretKeyFactory skf = SecretKeyFactory.getInstance("PBEWithSHA1AndDESede");
	byte[] hash = skf.generateSecret(spec).getEncoded();
	return hash;
        }
    catch( NoSuchAlgorithmException | InvalidKeySpecException e ) {
        throw new RuntimeException( e );
    }
}
	
    public String getSalt() {
        return new BigInteger(140, random).toString(32);
    }
}
