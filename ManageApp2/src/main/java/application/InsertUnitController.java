/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import tools.Connection;
import tools.LandLord;
import tools.Property;
import tools.Switch;

/**
 * FXML Controller class
 *
 * @author tcvsh
 */
public class InsertUnitController extends Connection {
    
    @FXML
    private TextField number;

    @FXML
    private TextField rent;

    @FXML
    private Button add;

    @FXML
    private Button backButton;
    
    @FXML
    private Text prompt;
    
    private Property prop;
    private LandLord owner;
    
    public void setData(Property prop, LandLord owner){
        this.owner = owner;
        this.prop = prop;
    }
    
    public void Back(ActionEvent event) throws IOException{
        //Passing data to other controller
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Units.fxml"));
        Parent root = loader.load();
        UnitsController unitControl = loader.getController();
        unitControl.setData(this.prop, this.owner);
        
        //Switch Scene
        Switch sceneSwitcher = new Switch();
        sceneSwitcher.switchScene(event, "Units.fxml", root);
    }
    
    public void insert(ActionEvent event){
        try {
            String query = "INSERT INTO units VALUES(null, ?, ?, null, null, ?)";
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setInt(1, Integer.valueOf(this.number.getText()));
            statement.setInt(2, Integer.valueOf(this.prop.getPropertyID()));
            statement.setString(3, this.rent.getText());
            statement.executeUpdate();
            this.prompt.setText("Success, you can back off this page");
        } catch (SQLException ex) {
            Logger.getLogger(InsertUnitController.class.getName()).log(Level.SEVERE, null, ex);
            this.prompt.setText("error, failed to insert new unit");
        }
        
    }
}
