package application;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import tools.Connection;
import tools.Register;
import tools.Switch;

public class RegisterController extends Connection {
    
    @FXML
    private TextField username;

    @FXML
    private TextField firstname;

    @FXML
    private TextField lastname;

    @FXML
    private TextField email;

    @FXML
    private PasswordField password;

    @FXML
    private Button registerButton;
    
    @FXML
    private Text promptMessage;
    
    public void register(){
        Register register = new Register(getConnection());
        register.registerUser(password.getText(), username.getText(), lastname.getText(), firstname.getText(), email.getText(), promptMessage);
    }
    
    public void Back(ActionEvent event){
        Switch sceneSwitcher = new Switch();
        sceneSwitcher.switchScene(event, "login.fxml");
    }
}
