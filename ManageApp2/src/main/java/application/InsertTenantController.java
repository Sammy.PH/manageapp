/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import tools.*;
/**
 * FXML Controller class
 *
 * @author tcvsh
 */
public class InsertTenantController implements Initializable {
    
    @FXML
    private AnchorPane anchorScene;
    
    @FXML
    private TextField firstName;

    @FXML
    private TextField lastName;

    @FXML
    private TextField phone;

    @FXML
    private TextField email;

    @FXML
    private Button pdfButton;

    @FXML
    private Button addButton;
    
    
    @FXML
    private Button backButton;

    @FXML
    private Text prompt;
    
    private Property prop;
    private LandLord owner;
    private Unit unit;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }
    
    public void setData(Property prop, LandLord owner, Unit unit){
        this.owner = owner;
        this.prop = prop;
        this.unit = unit;
        FileChooser fileChooser = new FileChooser();
        this.pdfButton.setOnAction(e -> {
            Stage stage = (Stage) this.anchorScene.getScene().getWindow();
            File selectedFile = fileChooser.showOpenDialog(stage);
        });
    }
    
    public void Back(ActionEvent event) throws IOException{
                //Passing data to other controller
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Units.fxml"));
        Parent root = loader.load();
        UnitsController unitControl = loader.getController();
        unitControl.setData(this.prop, this.owner);
        
        //Switch Scene
        Switch sceneSwitcher = new Switch();
        sceneSwitcher.switchScene(event, "Units.fxml", root);
    }
    
    
    public void addTenant(ActionEvent event){
        int unitId = this.unit.getUnitId();
        
    }
}
