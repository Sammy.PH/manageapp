package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import tools.Institution;
import tools.LandLord;
import tools.Mortgage;
import tools.Switch;

public class MortgageController implements Initializable {


    @FXML
    private Text headerText;

    @FXML
    private Text startText;

    @FXML
    private Text endText;

    @FXML
    private Button changeButton;

    @FXML
    private Button renewButton;

    @FXML
    private Button backButton;
    
    private LandLord owner;
    private Institution inst;
    private Mortgage mortgage;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    
    public void setData(LandLord owner, Institution inst, Mortgage mortgage){
        this.owner = owner;
        this.inst = inst;
        this.mortgage = mortgage;
        
        
        this.headerText.setText("Mortgage for property: " + this.mortgage.getPropId());
        this.startText.setText("Start date: " + this.mortgage.getStartDate());
        this.endText.setText("End Date: " + this.mortgage.getEndDate());
        
    }
    
    public void Back(ActionEvent event) throws IOException{
        //simple validation
        if(!(this.owner == null)){
            
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Institution.fxml"));
            Parent root = loader.load();
            InstitutionController instController = loader.getController();
            instController.setData(this.owner);
            //Switching scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "Institution.fxml", root);
        }
    }
}
