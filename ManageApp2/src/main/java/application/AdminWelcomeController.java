
package application;

import java.io.IOException;
import tools.Connection;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import tools.Switch;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import tools.LandLord;
import tools.Property;

public class AdminWelcomeController extends Connection{
        
    @FXML
    private Button backButton;
    
    @FXML
    private Text welcomeText;
    
    private LandLord owner;
    
    
    public void Back(ActionEvent event){
        Switch sceneSwitcher = new Switch();
        sceneSwitcher.switchScene(event, "login.fxml");
    }
    
    public void ForwardtoProperties(ActionEvent event) throws IOException{
        
        //simple validation
        if(!(this.owner == null)){
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("properties.fxml"));
            Parent root = loader.load();
            PropertiesController props = loader.getController();
            props.setData(owner);
            
            //switching scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "properties.fxml", root);
        }
    }
    
    public void ForwardtoInstitution(ActionEvent event) throws IOException{
        //Simple validation for if there is a selected row
        if(!(this.owner == null)){
            
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("institution.fxml"));
            Parent root = loader.load();
            InstitutionController instController = loader.getController();
            instController.setData(this.owner);
            //Switching Scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "institution.fxml", root);
        }
    }
    
    public void setData(LandLord owner){
        this.owner = owner;
        welcomeText.setText(owner.getName());
    }
}
