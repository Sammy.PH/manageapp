/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import tools.Switch;

/**
 * FXML Controller class
 *
 * @author Sheidez
 */
public class LeaseController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void Back(ActionEvent event){
        Switch sceneSwitcher = new Switch();
        sceneSwitcher.switchScene(event, "tenantWelcome.fxml");
}
}
