package application;

import tools.Connection;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import tools.Connection;
import tools.Login;
import tools.Switch;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import tools.LandLord;
import tools.Tenant;

public class LoginController extends Connection { 
    @FXML
    private TextField user;

    @FXML
    private PasswordField psswd;
    
    @FXML
    private Text errorText;

    public void loginAttempt(ActionEvent event) throws IOException{
        
        Login login = new Login(getConnection());
        String username = user.getText();
        String password = psswd.getText();
        //This line will create a login object
        int verified = login.validate(username, password);
        
        //if verified is equal to 1, then it is a tenant, 5 for the owner and 0 is invalid
        if(verified == 1){
            //Create tenant object to passs to the tenantWelcome controller
            Tenant tenant = new Tenant(getConnection(), username);
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("tenantWelcome.fxml"));
            Parent root = loader.load();
            TenantWelcomeController tenantController = loader.getController();
            tenantController.setData(tenant);
            //switching scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "tenantWelcome.fxml", root);
        }
        else if(verified == 5){
            
            //Create landlord object to pass to the adminwelcome controller
            LandLord owner = new LandLord(getConnection(), username);
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("adminWelcome.fxml"));
            Parent root = loader.load();
            AdminWelcomeController adminController = loader.getController();
            adminController.setData(owner);
            //switching scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "adminWelcome.fxml", root);
        }
        else{
            errorText.setText("Invalid credentials, Please try again");
        }
}
    public void registerClick(ActionEvent event){
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "Register.fxml");
    }
}
