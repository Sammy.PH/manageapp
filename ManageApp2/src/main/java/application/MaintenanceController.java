/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import tools.*;

public class MaintenanceController extends Connection implements Initializable {
    @FXML
    private TableView<Maintenance> table;

    @FXML
    private TableColumn<Maintenance, String> maintenanceCol;

    @FXML
    private TableColumn<Maintenance, String> issueCol;

    @FXML
    private TableColumn<Maintenance, String> starCol;

    @FXML
    private TableColumn<Maintenance, String> endCol;

    @FXML
    private TableColumn<Maintenance, String> ContractorCol;

    @FXML
    private TableColumn<Maintenance, String> PhoneCol;
    
    private LandLord owner;
    private Unit unit;
    private Property prop;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void Back(ActionEvent event) throws IOException{
        //Passing data to other controller
        FXMLLoader loader = new FXMLLoader(getClass().getResource("specificUnit.fxml"));
        Parent root = loader.load();
        SpecificUnitController unitControl = loader.getController();
        unitControl.setData(this.unit, this.prop,this.owner);
            
        //Switching scene
        Switch sceneSwitcher = new Switch();
        sceneSwitcher.switchScene(event, "specificUnit.fxml", root);
    }
    
    public void setData(Property prop, LandLord owner, Unit unit){
        this.prop = prop;
        this.unit = unit;
        this.owner = owner;
        try {
            
            String query = "SELECT units.unit_id, maintenance.maintenance_id, maintenance.issue_type, maintenance.date_issue, maintenance.date_fixed, contractors.name, contractors.phone_number FROM units\n" +
                    "JOIN maintenanceunits ON units.unit_id = maintenanceunits.unit_id\n" +
                    "JOIN maintenance ON maintenanceunits.maintenance_id = maintenance.maintenance_id\n" +
                    "JOIN contractors ON maintenance.maintenance_id = contractors.maintenance_id\n" +
                    "WHERE units.unit_id = ?";
            //Creating an observable list for the table to render
            ObservableList<Maintenance> maintenanceList = FXCollections.observableArrayList();
            PreparedStatement fetchMaintenance = getConnection().prepareStatement(query);
            fetchMaintenance.setString(1, Integer.toString(this.unit.getUnitId()));
            ResultSet rs = fetchMaintenance.executeQuery();
            while(rs.next()){
                Maintenance maintain = new Maintenance(
                        rs.getString("unit_id"),
                        rs.getString("maintenance_id"),
                        rs.getString("issue_type"),
                        rs.getString("date_issue"),
                        rs.getString("date_fixed"),
                        rs.getString("name"),
                        rs.getString("phone_number"));
                
                maintenanceList.add(maintain);
            }
            rs.close();
            fetchMaintenance.close();
            
            //Setting the table
            this.maintenanceCol.setCellValueFactory(new PropertyValueFactory<Maintenance, String>("unitId"));
            this.issueCol.setCellValueFactory(new PropertyValueFactory<Maintenance, String>("maintenanceId"));
            this.starCol.setCellValueFactory(new PropertyValueFactory<Maintenance, String>("dateStar"));
            this.endCol.setCellValueFactory(new PropertyValueFactory<Maintenance, String>("dateResolve"));
            this.ContractorCol.setCellValueFactory(new PropertyValueFactory<Maintenance, String>("name"));
            this.PhoneCol.setCellValueFactory(new PropertyValueFactory<Maintenance, String>("phone"));
            this.table.setItems(maintenanceList);
        } catch (SQLException ex) {
            Logger.getLogger(MaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
