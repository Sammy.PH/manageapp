/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import tools.Connection;
import tools.LandLord;
import tools.Property;
import tools.Switch;
import tools.Unit;


public class UnitsController extends Connection{
    
    @FXML
    private TableView<Unit> table;
    
    @FXML
    private TableColumn<Unit, String> unitCol;

    @FXML
    private TableColumn<Unit, String> tenantCol;

    @FXML
    private TableColumn<Unit, String> emailCol;

    @FXML
    private TableColumn<Unit, Boolean> paidCol;

    @FXML
    private TableColumn<Unit, String> LeaseExpCol;
    
    @FXML
    private Button unitButton;
    
    @FXML
    private Button insertUnit;

    @FXML
    private Button deleteUnit;
    
    @FXML
    private Button tenantClick;
    
    private Property prop;
    private LandLord owner;

    public void setData(Property prop, LandLord owner){
        this.prop = prop;
        this.owner = owner;
        //Creating an observable list for the table to render
        ObservableList<Unit> unitList = FXCollections.observableArrayList();
        try {
            String query = "SELECT units.unit_id, CONCAT_WS(' ', tenants.first_name, tenants.last_name) AS name, tenants.email , payments.paid, lease.end_date, tenants.phone_number, tenants.overdue_days FROM units\n" +
                            "JOIN tenants ON units.unit_id = tenants.unit_id\n" +
                            "JOIN payments ON tenants.tenant_id = payments.tenant_id\n" +
                            "JOIN lease ON payments.tenant_id = lease.tenant_id\n" +
                            "WHERE units.property_id = ?";
            PreparedStatement fetchUnit = getConnection().prepareStatement(query);
            fetchUnit.setString(1, this.prop.getPropertyID());
            ResultSet rs = fetchUnit.executeQuery();
            int counter = 0;
            while(rs.next()){
                Unit units = new Unit(
                                            rs.getInt("unit_id"),
                                            rs.getString("name"),
                                            rs.getString("email"),
                                            rs.getString("end_date"),
                                            rs.getBoolean("paid"),
                                            rs.getString("phone_number"),
                                            rs.getString("overdue_days"));
                unitList.add(units);
                System.out.println(counter);
                counter++;
            }
            rs.close();
            fetchUnit.close();
            
                        
            //Setting up the table
            unitCol.setCellValueFactory(new PropertyValueFactory<Unit, String>("unitId"));
            tenantCol.setCellValueFactory(new PropertyValueFactory<Unit, String>("tenantName"));
            emailCol.setCellValueFactory(new PropertyValueFactory<Unit, String>("email"));
            LeaseExpCol.setCellValueFactory(new PropertyValueFactory<Unit, String>("leaseDate"));
            paidCol.setCellValueFactory(new PropertyValueFactory<Unit, Boolean>("paid"));
            this.table.setItems(unitList);
        } catch (SQLException ex) {
            Logger.getLogger(PropertiesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void Back(ActionEvent event) throws IOException{
        //simple validation
        if(!(this.owner == null)){
            
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("properties.fxml"));
            Parent root = loader.load();
            PropertiesController props = loader.getController();
            props.setData(this.owner);
            //Switching Scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "properties.fxml", root);
        }
}
    
    public void unitClick(ActionEvent event)throws IOException{
        Unit unit = table.getSelectionModel().getSelectedItem();
        //Simple validation for if there is a selected row
        if(!(unit == null)){
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("specificUnit.fxml"));
            Parent root = loader.load();
            SpecificUnitController unitControl = loader.getController();
            unitControl.setData(unit, this.prop, this.owner);
            //Switching scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "specificUnit.fxml", root);
        }
    }
    
    
    public void deleteUnit(ActionEvent event) throws SQLException{
        Unit unit = table.getSelectionModel().getSelectedItem();
        unit.deleteUnit();
        table.getItems().remove(unit);
        unit=null;
    }
    
    public void insertUnit(ActionEvent event) throws IOException{
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("InsertUnit.fxml"));
            Parent root = loader.load();
            InsertUnitController unitControl = loader.getController();
            unitControl.setData(prop, this.owner);
            
            //Switching scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "InsertUnit.fxml", root);
    }
    
    public void insertTenant(ActionEvent event) throws IOException{
            Unit unit = table.getSelectionModel().getSelectedItem();
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("InsertTenant.fxml"));
            Parent root = loader.load();
            InsertTenantController unitControl = loader.getController();
            unitControl.setData(this.prop, this.owner, unit);
            
            //Switching scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "InsertTenant.fxml", root);
    }
}
