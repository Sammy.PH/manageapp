package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import tools.LandLord;
import tools.Property;
import tools.Switch;
import tools.Unit;

public class SpecificUnitController implements Initializable {
    
    @FXML
    private Text unitText;

    @FXML
    private Text nameText;

    @FXML
    private Button leaseButton;

    @FXML
    private Button contactButton;

    @FXML
    private Text phoneText;

    @FXML
    private Text overdueText;

    @FXML
    private Button courtButton;

    private Property prop;
    private Unit unit;
    private LandLord owner;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    
    public void setData(Unit unit, Property prop, LandLord owner){
        this.unit = unit;
        this.prop = prop;
        this.owner = owner;
        
        this.nameText.setText(this.unit.getTenantName());
        this.unitText.setText(Integer.toString(this.unit.getUnitId()));
        this.phoneText.setText(this.unit.getPhone());
        if(this.unit.getOverdue().equals("")){
            this.overdueText.setText("Days overdue: not overdue");
        } else if(this.unit.getOverdue().equals("today")){
            this.overdueText.setText("Days overdue: today");
        }
        else{
            this.overdueText.setText("Days overdue: " + this.unit.getOverdue());
        }
        
    }
    
    public void Back(ActionEvent event) throws IOException{
        //Simple validation
        if(!(this.unit == null)){
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Units.fxml"));
            Parent root = loader.load();
            UnitsController unitControl = loader.getController();
            unitControl.setData(this.prop, this.owner);
        
        //Switch Scene
        Switch sceneSwitcher = new Switch();
        sceneSwitcher.switchScene(event, "Units.fxml", root);
        }
    }   
    
    public void maintenanceClick(ActionEvent event) throws IOException{
        //Simple validation
        if(!(this.unit == null)){
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("maintenance.fxml"));
            Parent root = loader.load();
            MaintenanceController maintControl = loader.getController();
            maintControl.setData(this.prop, this.owner, this.unit);
        
            //Switch Scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "maintenance.fxml", root);
        }
    }
}
