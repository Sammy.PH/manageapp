/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import tools.Connection;
import tools.Institution;
import tools.LandLord;
import tools.Mortgage;
import tools.Property;
import tools.Switch;

public class InstitutionController extends Connection implements Initializable {
    
    @FXML
    private TableView<Institution> table;

    @FXML
    private TableColumn<Institution, String> propCol;

    @FXML
    private TableColumn<Institution, String> nameCol;

    @FXML
    private TableColumn<Institution, String> phoneCol;

    @FXML
    private TableColumn<Institution, String> rateCol;
    
    @FXML
    private Button mortgageButton;
    
    @FXML
    private Button backButton;
    
    private LandLord owner;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<Institution> mortgageList = FXCollections.observableArrayList();
        
        try {
            //Query to database
            String query = "SELECT properties.property_id, properties.address,institution.institution_id, institution.name, institution.phone_number, institution.interest_rate, mortgage.end_date, mortgage.start_date " + 
                            "FROM properties JOIN mortgage ON properties.mortgage_id = mortgage.mortgage_id JOIN institution ON mortgage.institution_id = institution.institution_id";
            
            PreparedStatement fetchMortgage = getConnection().prepareStatement(query);
            ResultSet rs = fetchMortgage.executeQuery();
            
            //Creating an observable list for the table to render
            while(rs.next()){
                Institution inst = new Institution(
                                                 rs.getString("property_id"),
                                                 rs.getString("name"),
                                                 rs.getString("address"),
                                                 rs.getString("phone_number"),
                                                 rs.getString("interest_rate"));
                mortgageList.add(inst);
            }
            rs.close();
            fetchMortgage.close();
            
            //Setting up the table
            propCol.setCellValueFactory(new PropertyValueFactory<Institution, String>("property"));
            nameCol.setCellValueFactory(new PropertyValueFactory<Institution, String>("name"));
            phoneCol.setCellValueFactory(new PropertyValueFactory<Institution, String>("phone"));
            rateCol.setCellValueFactory(new PropertyValueFactory<Institution, String>("rate"));
            this.table.setItems(mortgageList);
        } catch (SQLException ex) {
            Logger.getLogger(InstitutionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }    
    
    public void Back(ActionEvent event) throws IOException{
        //simple validation
        if(!(this.owner == null)){
            
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("adminWelcome.fxml"));
            Parent root = loader.load();
            AdminWelcomeController admin = loader.getController();
            admin.setData(this.owner);
            //Switching scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "adminWelcome.fxml", root);
        }
    }
    
    public void setData(LandLord owner){
            this.owner = owner;
    }
    
    public void mortgageClick(ActionEvent event) throws IOException{
        Institution inst = table.getSelectionModel().getSelectedItem();
        //simple validation
        if(!(inst == null)){
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Mortgage.fxml"));
            Parent root = loader.load();
            MortgageController mortController = loader.getController();
            mortController.setData(this.owner, inst, selectMortgage(inst));
            //Switching scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "Mortgage.fxml", root);
        }
    }
    
    public Mortgage selectMortgage(Institution inst){
        try {
            int propId = Integer.valueOf(inst.getProperty());
            String query = "SELECT mortgage_id, institution_id, start_date, end_date FROM mortgage WHERE property_id = ?";
            PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setInt(1, propId);
            ResultSet rs = statement.executeQuery();
            rs.next();
            int mortgageId = rs.getInt("mortgage_id");
            int instId = rs.getInt("institution_id");
            String startDate = rs.getString("start_date");
            String endDate = rs.getString("end_date");
            Mortgage mortgage = new Mortgage(mortgageId, instId, startDate, endDate, propId);
            System.out.println(mortgage.getPropId());
            rs.close();
            statement.close();
            return mortgage;
        } catch (SQLException ex) {
            Logger.getLogger(InstitutionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
