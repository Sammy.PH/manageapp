/*
 * To change this license header, choose License Headers in Project Property.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.io.IOException;
import tools.Connection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.IntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import tools.LandLord;
import tools.Property;
import tools.Switch;
import tools.Tenant;

public class PropertiesController extends Connection{
    @FXML
    private TableView<Property> table;
    
    @FXML
    private TableColumn<Property, String> propID;

    @FXML
    private TableColumn<Property, String> address;

    @FXML
    private TableColumn<Property, String> postal;

    @FXML
    private TableColumn<Property, String> mortgage;

    @FXML
    private TableColumn<Property, String> numUnits;

    @FXML
    private TableColumn<Property, String> value;
    
    @FXML
    private Button unitButton;
    
    private LandLord owner;
    
    public void Back(ActionEvent event) throws IOException{
        
        //simple validation
        if(!(this.owner == null)){
            
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("adminWelcome.fxml"));
            Parent root = loader.load();
            AdminWelcomeController admin = loader.getController();
            admin.setData(this.owner);
            //Switching scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "adminWelcome.fxml", root);
        }
    }
    
    
    public void setData(LandLord owner){
        this.owner = owner;
        
        //Creating an observable list for the table to render
        ObservableList<Property> propList = FXCollections.observableArrayList();
        try {
            String query = "SELECT * FROM properties";
            PreparedStatement fetchProperty = getConnection().prepareStatement(query);
            ResultSet rs = fetchProperty.executeQuery();
            while(rs.next()){
                Property prop = new Property(
                                            rs.getString("property_id"),
                                            rs.getString("address"),
                                            rs.getString("postal_code"),
                                            Integer.toString(rs.getInt("mortgage_id")),
                                            rs.getString("number_of_unit"),
                                            rs.getString("property_value"));
                propList.add(prop);
            }
            rs.close();
            fetchProperty.close();
            
            //Setting up the table
            propID.setCellValueFactory(new PropertyValueFactory<Property, String>("propertyID"));
            address.setCellValueFactory(new PropertyValueFactory<Property, String>("address"));
            postal.setCellValueFactory(new PropertyValueFactory<Property, String>("postalCode"));
            mortgage.setCellValueFactory(new PropertyValueFactory<Property, String>("mortgageId"));
            numUnits.setCellValueFactory(new PropertyValueFactory<Property, String>("noOfUnit"));
            value.setCellValueFactory(new PropertyValueFactory<Property, String>("propertyValue"));
            this.table.setItems(propList);
            
        } catch (SQLException ex) {
            Logger.getLogger(PropertiesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void unitClick(ActionEvent event)throws IOException{
        
        Property prop = table.getSelectionModel().getSelectedItem();
        
        //Simple validation for if there is a selected row
        if(!(prop == null)){
            
            //Passing data to other controller
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Units.fxml"));
            Parent root = loader.load();
            UnitsController unitControl = loader.getController();
            unitControl.setData(prop, this.owner);
            
            //Switching scene
            Switch sceneSwitcher = new Switch();
            sceneSwitcher.switchScene(event, "Units.fxml", root);
        }
    }
    
    public void addClick(ActionEvent event) throws IOException{
        //Passing data to other controller
        FXMLLoader loader = new FXMLLoader(getClass().getResource("addProperty.fxml"));
        Parent root = loader.load();
        AddPropertyController propControl = loader.getController();
        propControl.setData(this.owner);
            
        //Switching scene
        Switch sceneSwitcher = new Switch();
        sceneSwitcher.switchScene(event, "addProperty.fxml", root);
    }
    
    public void deleteClick(ActionEvent event){
        try {
            Property prop = table.getSelectionModel().getSelectedItem();
            prop.deleteProperty();
            table.getItems().remove(prop);
            prop = null;
        } catch (SQLException ex) {
            Logger.getLogger(PropertiesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
