package application;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import tools.*;

public class AddPropertyController extends Connection implements Initializable {
    
    
    @FXML
    private TextField addressInput;

    @FXML
    private TextField untisInput;

    @FXML
    private TextField postalInput;

    @FXML
    private TextField valueInput;

    @FXML
    private TableView<Institution> table;

    @FXML
    private TableColumn<Institution, String> rateCol;

    @FXML
    private TableColumn<Institution, String> nameCol;

    @FXML
    private TableColumn<Institution, String> phoneCol;

    @FXML
    private Button addButton;

    @FXML
    private Button backButton;

    @FXML
    private Text statusText;
    
    private LandLord owner;
    private Mortgage mortgage;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<Institution> mortgageList = FXCollections.observableArrayList();
        try {
            //Query to database
            String query = "SELECT institution_id, name, phone_number, interest_rate FROM institution";
            
            PreparedStatement fetchMortgage = getConnection().prepareStatement(query);
            ResultSet rs = fetchMortgage.executeQuery();
            
            //Creating an observable list for the table to render
            while(rs.next()){
                Institution mortgage = new Institution(
                        rs.getInt("institution_id"),
                        rs.getString("name"),
                        rs.getString("phone_number"),
                        rs.getString("interest_rate"));
                mortgageList.add(mortgage);
            }
            rs.close();
            fetchMortgage.close();
            
            //Setting up the table
            nameCol.setCellValueFactory(new PropertyValueFactory<Institution, String>("name"));
            phoneCol.setCellValueFactory(new PropertyValueFactory<Institution, String>("phone"));
            rateCol.setCellValueFactory(new PropertyValueFactory<Institution, String>("rate"));
            this.table.setItems(mortgageList);
        } catch (SQLException ex) {
            Logger.getLogger(AddPropertyController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    
    public void setData(LandLord owner){
        this.owner = owner;
    }
    
    public void Back(ActionEvent event) throws IOException{
        //Passing data to other controller
        FXMLLoader loader = new FXMLLoader(getClass().getResource("properties.fxml"));
        Parent root = loader.load();
        PropertiesController propControl = loader.getController();
        propControl.setData(this.owner);
            
        //Switching scene
        Switch sceneSwitcher = new Switch();
        sceneSwitcher.switchScene(event, "properties.fxml", root);
    }
    
    public void clickAdd(ActionEvent event) throws SQLException{
        Institution inst = table.getSelectionModel().getSelectedItem();
        //Inserting a row to the mortgage table
        Mortgage mortgage = new Mortgage(inst.getInstitutionId());
        String mortgageId = Integer.toString(mortgage.addMortgage());
        Property prop = new Property(null, this.addressInput.getText(), this.postalInput.getText(), mortgageId, this.untisInput.getText(), this.valueInput.getText());
        //Inserting new property
        String propQuery = "INSERT INTO properties VALUES( ?, ?, ?, ?, ?, ?)";
        PreparedStatement propStatement = getConnection().prepareStatement(propQuery, new String[]{"mortgage_id"});
        propStatement.setString(1, prop.getPropertyID());
        propStatement.setString(2, prop.getAddress());
        propStatement.setString(3, prop.getPostalCode());
        propStatement.setInt(4,Integer.parseInt(prop.getMortgageId()));
        propStatement.setString(5, prop.getNoOfUnit());
        propStatement.setString(6, prop.getPropertyValue());
        //see if there is a problem with the query
        int rowsChanged = propStatement.executeUpdate();
        if(rowsChanged == 0){
            throw new SQLException("Mortgage not added");
        }
        ResultSet propRS = propStatement.getGeneratedKeys();//Getting the id generated for the mortgage
        propRS.next();
        int propId = propRS.getInt(1);
        System.out.println(propId);
        //Changing the null value of mortgage table
        propRS.close();
        propStatement.close();
        String mortgageQuery = "UPDATE mortgage SET property_id = ? WHERE mortgage_id = ?";
        PreparedStatement mortgageAlter = getConnection().prepareStatement(mortgageQuery);
        mortgageAlter.setInt(1, propId);
        mortgageAlter.setInt(2, Integer.valueOf(mortgageId));
        mortgageAlter.executeUpdate();
        mortgageAlter.close();
        this.statusText.setText("Status: property added");
    }
}
