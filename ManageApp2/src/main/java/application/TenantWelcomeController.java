
package application;

import tools.Connection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import tools.Switch;
import tools.Tenant;


public class TenantWelcomeController extends Connection{
    private Tenant tenant;
    
    @FXML
    private Text welcomeText;

    public void setData(Tenant tenant){
        this.tenant = tenant;
        System.out.println(this.tenant.getName());
        String text = "Welcome " + this.tenant.getName() + "!";
        welcomeText.setText(text);
    }
    
    public void Back(ActionEvent event){
        Switch sceneSwitcher = new Switch();
        sceneSwitcher.switchScene(event, "login.fxml");
}
    
    public void ForwardtoPayments(ActionEvent event){
        Switch sceneSwitcher = new Switch();
        sceneSwitcher.switchScene(event, "PaymentLogs.fxml");
}
    
    public void ForwardtoLease(ActionEvent event){
        Switch sceneSwitcher = new Switch();
        sceneSwitcher.switchScene(event, "Lease.fxml");
}
}
