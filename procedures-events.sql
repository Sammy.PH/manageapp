/*Counter procedure will count the number of days the tenant is overdue*/
DELIMITER $$
CREATE PROCEDURE counter()
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE today date;
	DECLARE days int;
    DECLARE due_date date;
    DECLARE tenant varchar(7);
    DECLARE curMissed 
		CURSOR FOR
			SELECT payments.tenant_id, payments.due_date FROM payments WHERE paid = false;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	OPEN curMissed;
    SET today = current_date();
    
    getRows: LOOP
		FETCH curMissed INTO tenant, due_date;
        
        IF done THEN
			LEAVE getRows;
		END IF;
        SET days = timestampdiff(DAY, due_date, today);
        
        IF ( days >= 0 ) THEN
			IF ( days = 0 ) THEN
				UPDATE tenants SET overdue_days = "Today" WHERE tenants.tenant_id = tenant;
			ELSE
				UPDATE tenants SET overdue_days = CONVERT(days, CHAR) WHERE tenants.tenant_id = tenant;
            END IF;
            ELSE
				UPDATE tenants SET overdue_days = null WHERE tenants.tenants_id = tenant;
        END IF;
    END LOOP;
END $$

DELIMITER *$

CREATE PROCEDURE monthlyRent()
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE id VARCHAR(7);
    DECLARE rent VARCHAR(7);
    DECLARE tenantsCur CURSOR for
		SELECT tenants.tenant_id, units.rent FROM tenants JOIN units ON tenants.unit_id = units.unit_id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    OPEN tenantsCur;
    
	getTenant : LOOP
		FETCH tenantsCur INTO id, rent;
        
        IF done THEN
			LEAVE getTenant;
        END IF;
        
        /*Adding the 7 days to the current date(of which will be in 3 weeks) of when the procedure is called by the event*/
        INSERT INTO payments values(NULL, rent, DATE_ADD(CURRENT_DATE(), INTERVAL 7 DAY), id, false);
        
    END LOOP;
END *$


/*The verifyLogin procedure will verify if the user input correspond to a user on the database*/
DELIMITER //

CREATE PROCEDURE verifyLogin(
IN inputPassword VARCHAR(1000),
IN inputUsername VARCHAR(50), 
OUT verification INT)
BEGIN
    IF EXISTS(select * from user where (username = inputUsername) and (password = inputPassword) AND (role = "tenant")) THEN
        SET verification = 1;
    END IF;
	IF EXISTS(select * from user where (username = inputUsername) and (password = inputPassword) AND (role = "owner")) THEN
		SET verification = 5;
	ELSE
		SET verification = 0;
	END IF;
END //
DELIMITER ;

/*This even will run counter procedure everyday to update the days the tenant is overdue*/
DELIMITER |

CREATE EVENT OverdueCounterEvent
ON SCHEDULE EVERY 1 DAY
DO
	call javaproject.counter;
END |