INSERT INTO institution values(null,'scotia','5142939999',1.26);
INSERT INTO mortgage values(null,'0001','2000-01-31','2022-01-31',1);
INSERT INTO properties values(null,"19-6666 garnier", 'k0k0k0', 1, 20, 200000);
INSERT INTO payments values( NULL,'600', '2021-05-16', '1', true);
INSERT INTO lease values(Null, 1, '2020-06-12', '2021-06-12', false, '');
INSERT INTO user values(null, 'bob','lol', 'lol@gmail.com', 'tenant');
INSERT INTO user values(null, 'lol','bob', 'bob@gmail.com', 'owner');
INSERT INTO landlord values(null, 'bob', 'bobelle', 'bob@gmail.com', '5142296666', 2);
INSERT INTO tenants values(1, 'boby', 'bobelle', 'bob@gmail.com', '4382223333', 1, 1, 1, '');
INSERT INTO units values(1, 1, 1,1,1, '600');
INSERT INTO contractors values(1, 'bobinc', '5142223333', 1);
INSERT INTO maintenance values(1, 'toilet', 1, '2010-01-01-', '2010-02-01');
INSERT INTO propertiesunits values(1,1);
INSERT INTO maintenanceunits values(1, 1, 1);

/*Creating second placeholder values*/
INSERT INTO institution values(null,'td','5142939999','1.35');
INSERT INTO mortgage values(null,2,'2000-01-31','2022-01-31',2);
INSERT INTO properties values(null,"10-3245 rue scott", 'g2k1r2', 2, 20, 200000);
INSERT INTO payments values(NULL, '800', '2021-05-16', 2, false);
INSERT INTO lease values(2, 2, '2020-06-12', '2021-06-12', false, '');
INSERT INTO user values(NULL, 'rok','lol', 'lol@gmail.com', 'tenant');
INSERT INTO tenants values(Null, 'boby', 'bobelle', 'bob@gmail.com', '4382223333', 2, 2, 3, '');
INSERT INTO units values(Null, 2, 2, 2, 2, '800');
INSERT INTO contractors values(null, 'bobinc', '5142223333', 2);
INSERT INTO maintenance values(null, 'toilet', 2, '2010-01-01-', '2010-02-01');
INSERT INTO propertiesunits values(2,2);
INSERT INTO maintenanceunits values(2, 2, 2);