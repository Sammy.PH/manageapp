DROP TABLE `javaproject`.`contractors`, `javaproject`.`institution`, `javaproject`.`landlord`, `javaproject`.`lease`, `javaproject`.`maintenance`, `javaproject`.`maintenanceunits`, `javaproject`.`mortgage`, `javaproject`.`payments`, `javaproject`.`properties`, `javaproject`.`propertiesunits`, `javaproject`.`tenants`, `javaproject`.`units`, `javaproject`.`user`;
DROP PROCEDURE `javaproject`.`verifyLogin`;
DROP PROCEDURE counter;
DROP PROCEDURE monthlyRent;
DROP EVENT OverdueCounterEvent;
DROP EVENT newRent;